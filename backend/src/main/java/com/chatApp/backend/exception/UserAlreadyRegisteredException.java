package com.chatApp.backend.exception;

public class UserAlreadyRegisteredException extends RuntimeException {
    public UserAlreadyRegisteredException(String exception) {
        super(exception);
    }
}
