package com.chatApp.backend.exception;

public class WrongUserPinException extends RuntimeException {
    public WrongUserPinException(String exception) {
        super(exception);
    }
}
