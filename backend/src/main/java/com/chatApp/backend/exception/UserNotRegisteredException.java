package com.chatApp.backend.exception;

public class UserNotRegisteredException extends RuntimeException {
    public UserNotRegisteredException(String exception) {
        super(exception);
    }
}
