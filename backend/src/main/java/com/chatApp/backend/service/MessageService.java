package com.chatApp.backend.service;


import com.chatApp.backend.entity.Message;
import com.chatApp.backend.entity.User;
import com.chatApp.backend.exception.UserNotRegisteredException;
import com.chatApp.backend.model.MessageData;
import com.chatApp.backend.model.MessageRequest;
import com.chatApp.backend.repository.MessageRepository;
import com.chatApp.backend.repository.UserRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class MessageService {
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;

    MessageService(MessageRepository messageRepository, UserRepository userRepository) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
    }

    public MessageData postMessage(MessageRequest messageRequest) {
        Integer userId = fetchUserId(messageRequest.getUsername());

        Message message = new Message.MessageBuilder(messageRequest.getMessage(), userId, new Date()).build();
        messageRepository.save(message);

        return new MessageData(messageRequest.getUsername(), messageRequest.getMessage(), message.getTimestamp());
    }

    private Integer fetchUserId(String username) {
        User user = userRepository.findByUsername(username);
        if (user == null)
            throw new UserNotRegisteredException("User does not exist");
        return user.getId();
    }

    public List<MessageData> getAllMessages() {
        List<Message> messages = messageRepository.findAll(Sort.by("timestamp").ascending());
        List<Integer> userIds = messages.stream().map(Message::getUserId).collect(Collectors.toList());
        Map<Integer, String> userMap = userRepository.findAllById(userIds).stream().collect(
                Collectors.toMap(User::getId, User::getUsername));
        List<MessageData> responses = new ArrayList<>();
        messages.forEach(it -> responses.add(new MessageData(userMap.get(it.getUserId()), it.getMessage(), it.getTimestamp())));
        return responses;
    }
}
