package com.chatApp.backend.service;

import com.chatApp.backend.entity.User;
import com.chatApp.backend.exception.UserAlreadyRegisteredException;
import com.chatApp.backend.exception.UserNotRegisteredException;
import com.chatApp.backend.exception.WrongUserPinException;
import com.chatApp.backend.model.UserRequest;
import com.chatApp.backend.repository.UserRepository;
import lombok.NonNull;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;

    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void login(@NonNull UserRequest userRequest) {
        checkForEmpty(userRequest);
        User user = userRepository.findByUsername(userRequest.getUsername());

        if (user == null) {
            throw new UserNotRegisteredException("User does not exist");
        }
        if (!userRequest.getPin().equals(user.getPin())) {
            throw new WrongUserPinException("Wrong user pin");
        }
    }

    public void register(@NonNull UserRequest userRequest) {
        checkForEmpty(userRequest);
        User user = userRepository.findByUsername(userRequest.getUsername());

        if (user != null) {
            throw new UserAlreadyRegisteredException("User already exists, try logging in");
        }

        user = new User.UserBuilder(userRequest.getUsername(), userRequest.getPin()).build();
        user.setPin(userRequest.getPin());
        userRepository.save(user);
    }

    private void checkForEmpty(UserRequest request) {
        if (request.getUsername().trim().isEmpty()) {
            throw new IllegalArgumentException("User name cannot be empty");
        }
        if (request.getPin().trim().isEmpty()) {
            throw new IllegalArgumentException("Pin cannot be empty");
        }
    }
}
