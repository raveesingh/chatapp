package com.chatApp.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Getter
@Setter
public class Message {
    @Id
    @GeneratedValue
    private Integer id;
    private Integer userId;
    private String message;
    private Date timestamp;

    @AllArgsConstructor
    public static class MessageBuilder {
        private String text;
        private Integer userId;
        private Date timestamp;

        public Message build() {
            Message message = new Message();
            message.setMessage(text);
            message.setUserId(userId);
            message.setTimestamp(timestamp);
            return message;
        }
    }
}
