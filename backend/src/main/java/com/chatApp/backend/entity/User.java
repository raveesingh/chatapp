package com.chatApp.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue
    private Integer id;

    @NonNull
    @Column(unique = true)
    private String username;

    @NonNull
    private String pin;

    @AllArgsConstructor
    public static class UserBuilder {
        private String username;
        private String pin;

        public User build() {
            User user = new User();
            user.setUsername(username);
            user.setPin(pin);
            return user;
        }
    }
}
