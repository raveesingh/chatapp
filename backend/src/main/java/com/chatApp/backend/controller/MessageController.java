package com.chatApp.backend.controller;

import com.chatApp.backend.exception.UserNotRegisteredException;
import com.chatApp.backend.model.MessageData;
import com.chatApp.backend.model.MessageRequest;
import com.chatApp.backend.model.MessageResponse;
import com.chatApp.backend.service.MessageService;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.Optional;

@Controller
public class MessageController {

    private final MessageService messageService;

    MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @MessageMapping("/postMessage")
    @SendTo("/messages")
    public ResponseEntity<MessageResponse> postMessage(@RequestBody @NonNull MessageRequest messageRequest) {
        MessageResponse response = new MessageResponse();

        if (messageRequest.getUsername() == null || messageRequest.getMessage() == null) {
            response.setError(Optional.of("Username or Message cannot be null"));
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        try {
            MessageData data = messageService.postMessage(messageRequest);
            response.setData(Optional.of(data));
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (UserNotRegisteredException e) {
            response.setError(Optional.of(e.getMessage()));
            return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/getAllMessages")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<MessageData> getAllMessages() {
        return messageService.getAllMessages();
    }
}
