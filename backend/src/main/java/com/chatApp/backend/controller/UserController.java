package com.chatApp.backend.controller;

import com.chatApp.backend.exception.UserAlreadyRegisteredException;
import com.chatApp.backend.exception.UserNotRegisteredException;
import com.chatApp.backend.exception.WrongUserPinException;
import com.chatApp.backend.model.UserRequest;
import com.chatApp.backend.service.UserService;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    private final UserService userService;

    UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody @NonNull UserRequest userRequest) {
        if (userRequest.getUsername() == null || userRequest.getPin() == null) {
            return userResponse("Username or Pin cannot be null", HttpStatus.BAD_REQUEST);
        }

        try {
            userService.login(userRequest);
            return userResponse("User logged in", HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return userResponse(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (WrongUserPinException e) {
            return userResponse(e.getMessage(), HttpStatus.FORBIDDEN);
        } catch (UserNotRegisteredException e) {
            return userResponse(e.getMessage(), HttpStatus.UNAUTHORIZED);
        }

    }

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody @NonNull UserRequest userRequest) {
        if (userRequest.getUsername() == null || userRequest.getPin() == null) {
            return userResponse("Username or Pin cannot be null", HttpStatus.BAD_REQUEST);
        }

        try {
            userService.register(userRequest);
            return userResponse("User successfully created", HttpStatus.CREATED);
        } catch (IllegalArgumentException | UserAlreadyRegisteredException e) {
            return userResponse(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    private ResponseEntity<String> userResponse(String message, HttpStatus httpStatus) {
        return new ResponseEntity<>(message.toUpperCase(), httpStatus);
    }
}
