package com.chatApp.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;


@AllArgsConstructor
@Getter
@NonNull
public class UserRequest {
    @NonNull()
    private String username;

    @NonNull
    private String pin;
}
