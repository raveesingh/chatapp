package com.chatApp.backend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@NoArgsConstructor
@Getter
@Setter
public class MessageResponse {
    private Optional<MessageData> data;
    private Optional<String> error;

}
