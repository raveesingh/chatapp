package com.chatApp.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@AllArgsConstructor
@ToString
@Getter
@Setter
public class MessageData {
    @NonNull
    private String username;
    @NonNull
    private String message;
    @NonNull
    private Date timestamp;
}
