package com.chatApp.backend.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
@Getter
public class MessageRequest {
    @NonNull
    private String username;
    @NonNull
    private String message;
}
