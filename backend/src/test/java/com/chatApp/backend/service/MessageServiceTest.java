package com.chatApp.backend.service;

import com.chatApp.backend.entity.Message;
import com.chatApp.backend.entity.User;
import com.chatApp.backend.exception.UserNotRegisteredException;
import com.chatApp.backend.model.MessageData;
import com.chatApp.backend.model.MessageRequest;
import com.chatApp.backend.repository.MessageRepository;
import com.chatApp.backend.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.stream.Collectors;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@SpringBootTest
class MessageServiceTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private MessageRepository messageRepository;

    private MessageService messageService;

    @BeforeEach
    public void setup() {
        messageService = new MessageService(messageRepository, userRepository);
    }

    @Test
    public void shouldPostMessageIfUserExists() {
        MessageRequest messageRequest = new MessageRequest("test", "test message");
        User expectedUser = TestDataProvider.buildUser();
        MessageData expectedData = TestDataProvider.buildMessageData();
        when(userRepository.findByUsername(messageRequest.getUsername())).thenReturn(expectedUser);

        MessageData data = messageService.postMessage(messageRequest);
        Assertions.assertEquals(expectedData.toString(), data.toString());
        verify(userRepository, times(1)).findByUsername(messageRequest.getUsername());
        verify(messageRepository, times(1)).save(any(Message.class));

    }

    @Test
    public void shouldNotPostMessageIfUserDoesNotExist() {
        MessageRequest messageRequest = new MessageRequest("test", "test message");
        when(userRepository.findByUsername(messageRequest.getUsername())).thenReturn(null);

        try {
            messageService.postMessage(messageRequest);
        } catch (UserNotRegisteredException e) {
            Assertions.assertEquals(e.getMessage(), "User does not exist");
            verify(userRepository, times(1)).findByUsername(messageRequest.getUsername());
            verify(messageRepository, times(0)).save(any(Message.class));
        }
    }

    @Test
    public void shouldGetAllMessagesInChronologicalOrder() {
        List<Message> expectedMessages = TestDataProvider.buildMessageList();
        List<User> expectedUsers = TestDataProvider.buildUserList();
        List<Integer> userIds = expectedUsers.stream().map(User::getId).collect(Collectors.toList());
        List<MessageData> expectedMessageDataList = TestDataProvider.buildMessageDataList();

        when(messageRepository.findAll(Sort.by("timestamp").ascending())).thenReturn(expectedMessages);
        when(userRepository.findAllById(userIds)).thenReturn(expectedUsers);

        List<MessageData> messageDataList = messageService.getAllMessages();

        Assertions.assertEquals(messageDataList.get(0).toString(), expectedMessageDataList.get(0).toString());
        Assertions.assertEquals(messageDataList.get(1).toString(), expectedMessageDataList.get(1).toString());
        verify(userRepository, times(1)).findAllById(userIds);
        verify(messageRepository, times(1)).findAll(Sort.by("timestamp").ascending());

    }
}