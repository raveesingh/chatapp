package com.chatApp.backend.service;

import com.chatApp.backend.entity.Message;
import com.chatApp.backend.entity.User;
import com.chatApp.backend.model.MessageData;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TestDataProvider {
    private static Date date = new Date();

    public static User buildUser() {
        User user = new User.UserBuilder("test", "1234").build();
        user.setId(1);
        return user;
    }

    public static User buildUser(Integer id, String username, String pin) {
        User user = new User.UserBuilder(username, pin).build();
        user.setId(id);
        return user;
    }

    public static Message buildMessage(Integer id, String text, Integer userId) {
        Message message = new Message.MessageBuilder(text, userId, date).build();
        message.setId(id);
        return message;
    }

    public static List<Message> buildMessageList() {
        List<Message> messages = new ArrayList<>();
        messages.add(buildMessage(1, "test message1", 2));
        messages.add(buildMessage(3, "test message2", 4));
        return messages;
    }

    public static List<User> buildUserList() {
        List<User> users = new ArrayList<>();
        users.add(buildUser(2, "user1", "1234"));
        users.add(buildUser(4, "user2", "abcd"));
        return users;
    }

    public static MessageData buildMessageData() {
        return new MessageData(
                "test",
                "test message",
                date);
    }

    public static List<MessageData> buildMessageDataList() {
        Message message1 = buildMessage(1, "test message1", 2);
        Message message2 = buildMessage(3, "test message2", 4);
        List<MessageData> messageDataList = new ArrayList<>();
        messageDataList.add(new MessageData("user1", message1.getMessage(), message1.getTimestamp()));
        messageDataList.add(new MessageData("user2", message2.getMessage(), message2.getTimestamp()));
        return messageDataList;
    }
}
