package com.chatApp.backend.service;

import com.chatApp.backend.entity.User;
import com.chatApp.backend.exception.UserAlreadyRegisteredException;
import com.chatApp.backend.exception.UserNotRegisteredException;
import com.chatApp.backend.exception.WrongUserPinException;
import com.chatApp.backend.model.UserRequest;
import com.chatApp.backend.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    private UserService userService;

    @BeforeEach
    public void setup() {
        userService = new UserService(userRepository);
    }

    @Test
    public void shouldRegisterIfUsernameAndPinIsProvidedAndUserDoesNotExist() {
        UserRequest userRequest = new UserRequest("test", "test pin");

        when(userRepository.findByUsername(userRequest.getUsername())).thenReturn(null);

        userService.register(userRequest);

        verify(userRepository, times(1)).findByUsername(userRequest.getUsername());
        verify(userRepository, times(1)).save(any(User.class));
    }

    @Test
    public void shouldNotRegisterIfUserAlreadyExist() {
        UserRequest userRequest = new UserRequest("test", "test pin");
        User expectedUser = TestDataProvider.buildUser();

        when(userRepository.findByUsername(userRequest.getUsername())).thenReturn(expectedUser);

        try {
            userService.register(userRequest);
        } catch (UserAlreadyRegisteredException e) {
            Assertions.assertEquals(e.getMessage(), "User already exists, try logging in");
            verify(userRepository, times(1)).findByUsername(userRequest.getUsername());
            verify(userRepository, times(0)).save(any(User.class));
        }
    }

    @Test
    public void shouldThrowErrorIfUsernameIsEmpty() {
        UserRequest userRequest = new UserRequest(" ", "test pin");

        try {
            userService.register(userRequest);
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals(e.getMessage(), "User name cannot be empty");
            verify(userRepository, times(0)).findByUsername(userRequest.getUsername());
            verify(userRepository, times(0)).save(any(User.class));
        }
    }

    @Test
    public void shouldThrowErrorIfPinIsEmpty() {
        UserRequest userRequest = new UserRequest("test", " ");

        try {
            userService.register(userRequest);
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals(e.getMessage(), "Pin cannot be empty");
            verify(userRepository, times(0)).findByUsername(userRequest.getUsername());
            verify(userRepository, times(0)).save(any(User.class));
        }
    }

    @Test
    public void shouldLoginIfCorrectUsernameAndPinIsProvided() {
        UserRequest userRequest = new UserRequest("test", "1234");
        User expectedUser = TestDataProvider.buildUser();

        when(userRepository.findByUsername(userRequest.getUsername())).thenReturn(expectedUser);

        userService.login(userRequest);

        verify(userRepository, times(1)).findByUsername(userRequest.getUsername());
    }

    @Test
    public void shouldNotLoginIfUserDoesNotExist() {
        UserRequest userRequest = new UserRequest("test", "1234");

        when(userRepository.findByUsername(userRequest.getUsername())).thenReturn(null);

        try {
            userService.login(userRequest);

        } catch (UserNotRegisteredException e) {
            Assertions.assertEquals(e.getMessage(), "User does not exist");
            verify(userRepository, times(1)).findByUsername(userRequest.getUsername());

        }

    }

    @Test
    public void shouldNotLoginIfPinIsIncorrect() {
        UserRequest userRequest = new UserRequest("test", "12");
        User expectedUser = TestDataProvider.buildUser();

        when(userRepository.findByUsername(userRequest.getUsername())).thenReturn(expectedUser);

        try {
            userService.login(userRequest);

        } catch (WrongUserPinException e) {
            Assertions.assertEquals(e.getMessage(), "Wrong user pin");
            verify(userRepository, times(1)).findByUsername(userRequest.getUsername());

        }

    }

    @Test
    public void shouldThrowErrorWhileLoginIfUsernameIsEmpty() {
        UserRequest userRequest = new UserRequest(" ", "test pin");

        try {
            userService.login(userRequest);
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals(e.getMessage(), "User name cannot be empty");
            verify(userRepository, times(0)).findByUsername(userRequest.getUsername());
        }
    }

    @Test
    public void shouldThrowErrorWhileLoginIfPinIsEmpty() {
        UserRequest userRequest = new UserRequest("test", " ");

        try {
            userService.login(userRequest);
        } catch (IllegalArgumentException e) {
            Assertions.assertEquals(e.getMessage(), "Pin cannot be empty");
            verify(userRepository, times(0)).findByUsername(userRequest.getUsername());
        }
    }

}