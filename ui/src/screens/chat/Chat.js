import React, {useEffect, useRef, useState} from 'react';
import './chat.scss';
import Header from "../../components/header/Header";
import Chatbox from "../../components/chatbox/Chatbox";
import RefreshIcon from './refresh.svg';
import Message from "../../components/message/Message";
import {getAllMessages} from "../../services/message";
import SockJsClient from 'react-stomp';
import hostname from "../../services/hostname";

const Chat = ({username, notifyMessage}) => {
    const [messages, setMessages] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [newMessage, setNewMessage] = useState("");
    const getNewMessages = () => {
        setLoading(true);
        getAllMessages().then((value) => {
            setMessages(value);
            setLoading(false);
        })
    }

    useEffect(() => {
        getNewMessages();
    }, [])

    let socketRef = useRef();
    const sendSock = (message) => {
        socketRef.sendMessage("/app/postMessage", JSON.stringify({
            username: username,
            message: message,
        }))
        return new Promise(resolve => resolve(true))
    }

    return (
        <>
            <Header title="Chat room" action={<img src={RefreshIcon} onClick={getNewMessages} alt="refresh"/>}/>
            <div className="chat-wrapper">
                {isLoading ? <div className="loading">Loading messages...</div> : <div className="messages app-padding">
                    {messages && messages.map((message, index) => {
                        return <Message key={`${index}-${message.username}`} message={message}
                                        isLatest={index === messages.length}
                                        isMyMessage={username === message.username}/>
                    })}
                </div>}
                <SockJsClient
                    url={`${hostname}/websocket-chat/`}
                    topics={['/messages']}
                    onMessage={(msg) => {
                        if (msg.statusCodeValue === 201) {
                            setMessages([
                                ...messages,
                                msg.body.data,
                            ])
                        } else {
                            notifyMessage('There was some problem in sending your message, please refresh page.')
                        }
                    }}
                    ref={(client) => {
                        socketRef = client
                    }}
                />
                <div className="chatbox-wrapper app-padding">
                    <Chatbox onSendMessage={sendSock} newMessage={newMessage} setNewMessage={setNewMessage}/>
                </div>
            </div>
        </>)
}


export default Chat;
