import Chat from "../chat/Chat";
import React, {useState} from "react";
import User from "../user/User";
import authentication from "../../services/authentication";
import Notifier from "../../components/notifier/Notifier";
import './home.scss';

export default () => {
    const [loggedinUser, setLoggedinUser] = useState(null);
    const [user, setUser] = useState({username: '', pin: ''});
    const [notifierMessage, setNotifierMessage] = useState('');

    const notifyMessage = (message) => {
        setNotifierMessage(message);
        setTimeout(() => {
            setNotifierMessage('')
        }, 2500)
    }

    return <div className="chat-room">
        {
            loggedinUser ? <Chat username={loggedinUser} notifyMessage={notifyMessage}/> : (
                <User
                    authenticate={(user, type) => {
                        authentication(user, type).then((response) => {
                            if (response.status === 201 || response.status === 200) {
                                setLoggedinUser(user.username);
                            } else {
                                response.text().then(value => {
                                    notifyMessage(value);
                                })
                            }
                        });
                    }}
                    setUser={setUser}
                    user={user}
                />
            )
        }
        <Notifier message={notifierMessage}/>
    </div>


}
