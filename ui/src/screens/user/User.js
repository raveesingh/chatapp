import React from 'react';
import './user.scss';
import Header from "../../components/header/Header";

const User = ({user, setUser, authenticate}) => {
    return (
        <>
            <Header title="Login to chat room" action={null}/>
            <div className="user-wrapper">
                <div className="userform">
                    <input className="auth-input" type="text" name="username" value={user.username} onChange={(event) => {
                        setUser({
                            ...user,
                            username: event.target.value
                        })
                    }} placeholder="Username"/>
                    <input className="auth-input" maxLength={4} type="text" name="pin" value={user.pin} onChange={(event) => {
                        setUser({
                            ...user,
                            pin: event.target.value
                        })
                    }} placeholder="4 digit pin"/>
                    <div className="button-container">
                        <button type="submit" className="button login-button"
                                onClick={() => authenticate(user, 'login')}
                        >
                            Login
                        </button>
                        <button type="submit" className="button register-button"
                                onClick={() => authenticate(user, 'register')}
                        >
                            Register
                        </button>
                    </div>
                </div>
            </div>
        </>)
}


export default User;
