import React from 'react';
import './notifier.scss';

export default ({message}) => {
    return (
        <>
            {message && (<div className="notifier">
                {message}
            </div>)}
        </>)
}
