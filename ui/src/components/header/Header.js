import React from 'react';
import './header.scss';

const Header = ({title, action}) => {
    return (<div className="header app-padding">
        <div>{title}</div>
        {action ? <div>{action}</div> : null}
    </div>)
}

export default Header;
