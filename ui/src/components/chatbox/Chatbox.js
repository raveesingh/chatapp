import React, {useState} from 'react';
import './chatbox.scss';
import SendIcon from './send.svg';

export default ({onSendMessage}) => {
    const [newMessage, setNewMessage] = useState("");

    return (
        <div className="chatbox">
            <div className="chat">
                <textarea
                    placeholder="Your message here..."
                    value={newMessage}
                    onChange={(event) => {
                        setNewMessage(event.target.value)
                    }}
                    name="chat"
                />
            </div>
            <div className="submit">
                <button type="submit" onClick={() => {
                    onSendMessage(newMessage).then(() => {
                        setNewMessage('');
                    });
                }}>
                    <img src={SendIcon} alt="send"/>
                </button>
            </div>
        </div>)
}
