import React, {useEffect, useRef} from 'react';
import './message.scss';

export default ({message: {message, username, timestamp}, isLatest, isMyMessage}) => {
    const elementRef = useRef();
    useEffect(() => {
        if (isLatest) {
            elementRef.current.scrollIntoView()
        }
    });
    return (
        <div ref={elementRef} className={`message-wrapper ${isMyMessage ? 'my-messages' : ''}`}>
            <div className="message-box">
                <div className="username">
                    {isMyMessage ? 'Me' : username}
                </div>
                <div className="message">
                    {message}
                </div>
                <div className="timestamp">
                    {timestamp && <small>{new Date(timestamp).toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'})}</small>}
                </div>
            </div>
        </div>
    )
}
