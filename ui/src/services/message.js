import hostname from "./hostname";

export const getAllMessages = async () => {
    const response = await fetch(`${hostname}/getAllMessages`);
    return response.json()
}
