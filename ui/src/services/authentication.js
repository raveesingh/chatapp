import hostname from "./hostname";

export default async (user, type) => {
    return await fetch(`${hostname}/${type}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
    });
}
