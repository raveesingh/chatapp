import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Home from "./screens/home/Home";

ReactDOM.render(
    <React.StrictMode>
        <Home />
    </React.StrictMode>,
    document.getElementById('root')
);
