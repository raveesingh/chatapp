#ChatApp by [Ravee](mailto:raveesingh140@gmail.com)

*Built with:*
- Java 11, Spring Boot, Gradle
- H2 in memory database
- ReactJS
- WebSocket


*How to run the app?*

```
docker-compose up -d --build

#visit
http://localhost:3000

```
*Create some users and try chatting :)*
